Changelog for Easy Access VSF
=============================

1.0.8 (2020-09-28)
------------------

- Ajout info traitement fichier facture pour aide debug

1.0.7 (2020-06-22)
------------------

- #7800 : Suppression des emails de rapport d'import

1.0.6 (2020-05-08)
------------------

- Review

1.0.5 (2020-05-04)
------------------

- Correctif variable destinataire de l'email.

1.0.4 (2020-05-04)
------------------

- Augmentation de la fréquence de la tâche CRON de mise à jour à 5 fois / jour.
- En option: traçabilité par email.

1.0.3 (2019-06-14)
------------------

- Correction de l'appel à Artisan dans le scheduler.

1.0.2 (2019-06-13)
------------------

- Correction de la compression du PDF des CGV.
- Enregistrement des erreurs d'import des factures dans les logs.

1.0.1 (2019-06-13)
------------------

- Correction du pattern de l'identifiant client dans le nom de facture.

1.0.0 (2019-06-13)
------------------

- First release.
