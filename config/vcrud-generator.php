<?php

return [

    /**
     * Guard name
     */
    'guard' => "auth",

    /**
     * Route name
     */
    'route-name' => "vcrud.",

    /**
     * Layout
     */
    'layout' => "layout.admin.app",

    'stub-directory' => dirname(__DIR__).'/resources/stubs/',

    /**
     * Enabled
     */
    'enable' => true,

    'data' => [
        [
            'namespace' => 'Admin\Parameters\VehicleTypes',
            'modelClassName' => 'App\Models\VehicleType',
            'viewBaseName' => 'admin.parameters.vehicle-types',
            'locked' => false,
            'cards' => [
                [
                    'id' => 'card-1',
                    'file-name' => 'author',
                    'card_trans_title' => 'carte_1',
                    'css_classes' => '',
                    'panel_css_classes' => 'col-sm-4 col-12',
                ],

                [
                    'id' => 'card-2',
                    'file-name' => 'options',
                    'card_trans_title' => 'carte_2',
                    'css_classes' => '',
                    'panel_css_classes' => 'col-sm-4 col-12',
                ],

                [
                    'id' => 'card-3',
                    'file-name' => 'comment',
                    'card_trans_title' => 'carte_3',
                    'css_classes' => '',
                    'panel_css_classes' => 'col-sm-4 col-12',
                ]
            ],
            'formsFields' => [
                [
                    'id' => 'group_id',
                    'name' => 'group_id',
                    'label' => 'Nom du groupe',
                    'type' => 'select',
                    'model' => "App\Models\Group",
                    'model_via' => 'designation',
                    'model_key' => 'id',
                    'relation_type' => null,
                    'required' => true,
                    'multiple' => false,
                    'classes' => 'select2',
                    'target' => 'main',
                    'card' => 'card-1',

                    'list_selected' => true,
                ],
                [
                    'id' => 'user_id',
                    'name' => 'user_id',
                    'label' => 'Nom du user',
                    'type' => 'select',
                    'model' => "App\Models\User",
                    'model_via' => 'username',
                    'model_key' => 'id',
                    'relation_type' => null,
                    'required' => true,
                    'multiple' => false,
                    'classes' => 'select2',
                    'target' => 'main',
                    'card' => 'card-1',
                ],
                [
                    'id' => 'role_id',
                    'name' => 'role_id',
                    'label' => 'Nom du role',
                    'type' => 'select',
                    'model' => "App\Models\Role",
                    'model_via' => 'name',
                    'model_key' => 'id',
                    'relation_type' => null,
                    'required' => true,
                    'multiple' => false,
                    'classes' => 'select2',
                    'target' => 'main',
                    'card' => 'card-1',
                ],
                [
                    'id' => 'first_name',
                    'name' => 'first_name',
                    'label' => 'Prénom',
                    'type' => 'text',
                    'model' => null,
                    'model_via' => null,
                    'model_key' => null,
                    'relation_type' => null,
                    'required' => false,
                    'multiple' => false,
                    'classes' => null,
                    'target' => 'main',
                    'card' => 'card-3',
                ],

                [
                    'id' => 'description',
                    'name' => 'description',
                    'label' => 'Description',
                    'type' => 'text',
                    'model' => null,
                    'model_via' => null,
                    'model_key' => null,
                    'relation_type' => null,
                    'required' => false,
                    'multiple' => false,
                    'classes' => null,
                    'target' => 'main',
                    'card' => 'card-3',
                ],

                [
                    'id' => 'is_valid',
                    'name' => 'is_valid',
                    'label' => 'Est valide',
                    'type' => 'checkbox',
                    'model' => null,
                    'model_via' => null,
                    'model_key' => null,
                    'relation_type' => null,
                    'required' => false,
                    'multiple' => false,
                    'classes' => null,
                    'target' => 'main',
                    'card' => 'card-2',
                ],

            ],
        ],
    ]


];
