<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vcrud', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->text('description')->nullable();
            $table->string('model_class_name');
            $table->string('view_base_name');
            $table->string('namespace');
            $table->softDeletes();
            $table->timestamps();

        });

        Schema::create('vcrud_forms_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vcrud_id');
            $table->foreign('vcrud_id')->references('id')->on('vcrud');

            $table->string('title_key');
            $table->string('file_name');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('vcrud_form_group_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vcrud_form_id');
            $table->foreign('vcrud_form_id')->references('id')->on('vcrud_forms_groups');

            $table->string('uid');
            $table->string('name');
            $table->string('label');
            $table->string('type');
            $table->string('model')->nullable();
            $table->string('model_via_key')->nullable();
            $table->string('model_via_label')->nullable();
            $table->boolean('multiple')->default(false)->nullable();
            $table->boolean('required')->default(true)->nullable();
            $table->boolean('css_classes')->default(true)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vcrud_form_group_fields');
        Schema::dropIfExists('vcrud_forms_groups');
        Schema::dropIfExists('vcrud');
    }
}
