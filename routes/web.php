<?php

use Illuminate\Support\Facades\Route;
use Foolskill\LaravelVCrudGenerator\Http\Controllers\ListController;

Route::name(config('vcrud-generator.route-name'))
    ->prefix('admin/vcrud')
    ->middleware(['web', config('vcrud-generator.guard')])
    ->group(function () {

        Route::get('list', [ListController::class, 'index'])
            ->name('list')
        ;
    });


