<?php

namespace App\Http\Controllers\[namespace];

use [modelClassNameWithNamespace];
use [managerClassname] as ModelManager;
use App\Http\Requests\[namespace]\UpdateRequest as Request;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{
    /**
     * Show the edit forms
     *
     * [modelClassName] $[modelClassNameToLower]
     */
    public function edit([modelClassName] $[modelClassNameToLower])
    {
        return view('[viewBaseName].edit', [
            '[modelClassNameToLower]' => $[modelClassNameToLower],
            'modelOptions' => ModelManager::viewsData(),
        ]);
    }

    /**
     * Update the existing Model
     *
     * @param Request $request
     * @param [modelClassName] $[modelClassNameToLower]
     * @param ModelManager $modelManager
     */
    public function update(Request $request, [modelClassName] $[modelClassNameToLower], ModelManager $modelManager): \Illuminate\Http\RedirectResponse
    {
        try {
            $modelManager->update($[modelClassNameToLower], $request);
            return back()->with('success', trans('[trans].updated'));
        } catch(\Throwable $e) {
            if (auth()->user()->is_super_admin) {
                return back()->with('error', $e->getMessage());
            }
            return back()->with('error', trans('navigation.error_500'));
        }
    }
}
