<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

use Foolskill\LaravelVCrudGenerator\Helper;

class RepositoryGenerator extends BaseGenerator implements Generator
{
    public function generate()
    {
        try {
            $directory = Helper::makeDirectory(app_path('Repositories'));
            $baseModelClassName = Helper::baseClassname($this->modelClassName);
            $template = str_replace(
                [
                    '[modelClassNameWithNamespace]',
                    '[modelClassName]',
                    '[repositoryClassName]',
                ],
                [
                    $this->modelClassName,
                    $baseModelClassName,
                    $baseModelClassName.'Repository',
                ],
                Helper::repositoryStub()
            );

            Helper::putContent("{$directory}/{$baseModelClassName}Repository.php", $template);

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }
    }
}
