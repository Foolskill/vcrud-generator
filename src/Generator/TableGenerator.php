<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;
use Foolskill\LaravelVCrudGenerator\Helper;

class TableGenerator extends BaseGenerator implements Generator
{

    public function generate()
    {
        // TODO: Implement generate() method.
        try {

            $model = $this->getModel();
            $this->fileManager->setPath(dirname($this->fileManager->getPath()));

            $thead = [];
            $filter = [];
            $td = [];

            foreach ($model->formFields as $formField) {

                $thead[] = str_replace(
                    '[text]',
                    sprintf("{!! trans('%s') !!}", "{$this->langPath}.{$formField->name}"),
                    Helper::getStubsContent('views/tables/th')
                );

                $td[] = str_replace(
                    '[text]',
                    '{!! $item->'.$formField->name.' !!}',
                    Helper::getStubsContent('views/tables/td')
                );

                $filter[] = str_replace(
                    '[text]',
                    $formField->label,
                    Helper::getStubsContent('views/tables/td')
                );
            }

            //dd($thead, $filter, $td);

            $template = str_replace(
                [
                    '[thead]',
                    '[filter]',
                    '[td]',
                    '[namespace]',
                    '[viewBaseName]',
                    '[modelClassName]',

                ],
                [
                    join("\t\t\t\t", $thead),
                    join("\t\t\t\t", $filter),
                    join("\t\t\t\t\t", $td),
                    $this->langPath,
                    $this->viewBaseName,
                    lcfirst($this->modelClassName),
                ],
                $this->fileManager->viewStub("list-table")
            );

            //dd($template);
            $this->fileManager->putOn("list-table.blade.php", $template);


        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }
    }

    private function generateTable($formType)
    {
        $labelTrans = str_replace('\\', '/', strtolower($this->namespace));
        $model = strtolower($this->modelClassName);
        return str_replace(
            [
                '[id]',
                '[name]',
                '[label]',
                '[value]',
                '[required]',
                '[multiple]',
                '[model]',
                '[class]',
            ],
            [
                $formType->getId(),
                $formType->getName(),
                sprintf('trans("%s")', "{$labelTrans}.{$formType->getName()}"),
                "$$model->{$formType->getName()}",
                $formType->getRequired() ? 'true' : 'false',
                $formType->getMultiple() ? 'true' : 'false',
                $formType->getRequired()
                    ? "{$formType->getModelClassName()}::pluck('name', '{$formType->getModelKey()}')"
                    : "{$formType->getModelClassName()}::pluck('name', '{$formType->getModelKey()}')->prepend('', null)",
                $formType->getClasses()
            ],
            $this->fileManager->viewStub("forms/{$formType->getType()}")
        );
    }
}
