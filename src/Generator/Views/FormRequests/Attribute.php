<?php

namespace Foolskill\LaravelVCrudGenerator\Generator\Views\FormRequests;

class Attribute
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $langPath = 'navigation';

    /**
     * @param string $langPath
     * @return self
     */
    public function setLangPath(string $langPath): self
    {
        $this->langPath = $langPath;
        return $this;
    }

    /**
     * @param string $key
     * @return $this
     */
    public function setKey(string $key): self
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function generate(): string
    {
        return sprintf(
            "'%s' => '« '.trans('%s').' »',\n",
            $this->key,
            $this->langPath.'.'.$this->key
        );
    }
}
