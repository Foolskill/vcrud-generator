<?php

namespace Foolskill\LaravelVCrudGenerator\Generator\Views\Forms;

class FormType
{
    protected $id;

    protected $name;

    protected $type;

    protected $label;

    protected $classes;

    protected $value;

    protected $multiple;

    protected $required;

    protected $modelClassName;

    protected $modelKey;

    protected $modelVia;

    /**
     * @param mixed $modelClass
     */
    public function setModelClassName($modelClass)
    {
        $this->modelClassName = $modelClass;
        return $this;
    }

    /**
     * @param mixed $modelKey
     */
    public function setModelKey($modelKey)
    {
        $this->modelKey = $modelKey;
        return $this;
    }

    /**
     * @param mixed $modelVia
     */
    public function setModelVia($modelVia)
    {
        $this->modelVia = $modelVia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModelVia()
    {
        return $this->modelVia;
    }

    /**
     * @return mixed
     */
    public function getModelKey()
    {
        return $this->modelKey;
    }

    /**
     * @return mixed
     */
    public function getModelClassName()
    {
        return $this->modelClassName;
    }

    /**
     * @param mixed $classes
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;
        return $this;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClasses()
    {
        return $this->classes;

    }

    /**
     * @param mixed $multiple
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
        return $this;
    }

    /**
     * @param mixed $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @return mixed
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }



}
