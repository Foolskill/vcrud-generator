<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

class CodeGenerator implements Generator
{
    private $controllerGenerator;

    private $formRequestGenerator;

    private $modelManagerGenerator;

    private $langueGenerator;

    private $viewGenerator;

    private $routeGenerator;

    private $repositoryGenerator;

    private $model;

    public function __construct()
    {
        $this->controllerGenerator = new ControllerGenerator();
        $this->formRequestGenerator = new FormRequestGenerator();
        $this->modelManagerGenerator = new ManagerGenerator();
        $this->repositoryGenerator = new RepositoryGenerator();
        $this->langueGenerator = new LangueGenerator();
        $this->viewGenerator = new ViewGenerator();
        $this->routeGenerator = new RouteGenerator();

    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    public function generate()
    {
        $this
            ->langueGenerator
            ->setModel($this->model)
            ->setModelClassName($this->model->modelClassName)
            ->setViewBaseName($this->model->viewBaseName)
            ->generate()
        ;

        $this
            ->formRequestGenerator
            ->setModel($this->model)
            ->setNamespace($this->model->namespace)
            ->setModelClassName($this->model->modelClassName)
            ->setViewBaseName($this->model->viewBaseName)
            ->generate()
        ;

        $this
            ->modelManagerGenerator
            ->setModel($this->model)
            ->setNamespace($this->model->namespace)
            ->setModelClassName($this->model->modelClassName)
            ->generate()
        ;

        $this
            ->repositoryGenerator
            ->setModel($this->model)
            ->setNamespace($this->model->namespace)
            ->setModelClassName($this->model->modelClassName)
            ->generate()
        ;

        $this
            ->controllerGenerator
            ->setModel($this->model)
            ->setNamespace($this->model->namespace)
            ->setModelClassName($this->model->modelClassName)
            ->setViewBaseName($this->model->viewBaseName)
            ->generate()
        ;

        $this
            ->routeGenerator
            ->setModel($this->model)
            ->setNamespace($this->model->namespace)
            ->setModelClassName($this->model->modelClassName)
            ->setViewBaseName($this->model->viewBaseName)
            ->generate()
        ;

        $this
            ->viewGenerator
            ->setModel($this->model)
            ->setNamespace($this->model->namespace)
            ->setModelClassName($this->model->modelClassName)
            ->setViewBaseName($this->model->viewBaseName)
            ->generate()
        ;

    }
}
