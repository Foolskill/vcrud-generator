<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

interface Generator
{
    /**
     * @return mixed
     */
    public function generate();
}
