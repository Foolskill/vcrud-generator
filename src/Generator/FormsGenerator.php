<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

use Foolskill\LaravelVCrudGenerator\Generator\views\forms\FormType;

class FormsGenerator extends BaseGenerator implements Generator
{

    public function generate()
    {
        // TODO: Implement generate() method.
        try {

            $model = $this->getModel();
            $this->fileManager->setPath($this->fileManager->getPath().'/partials/forms');

            foreach ($model->cards as $card) {

                $cardFileName = strtolower(str_replace(' ', '-', $card->name));
                $sources = [];
                foreach ($model->formFields as $formField) {

                    $formType = new FormType();
                    $formType
                        ->setId($formField->id)
                        ->setLabel($formField->label)
                        ->setName($formField->name)
                        ->setType($formField->type)
                        ->setMultiple($formField->multiple)
                        ->setRequired($formField->required)
                        ->setModelClassName($formField->model)
                        ->setModelKey($formField->model_key)
                        ->setModelVia($formField->model_via)
                        ->setClasses($formField->classes)
                    ;

                    $sources[] = $this->generateForms($formType);

                }

                //dd($cardFileName, $sources);
                $this->fileManager->putOn("{$cardFileName}.blade.php", join('', $sources));
            }


        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }
    }

    private function generateForms($formType)
    {
        $model = lcfirst($this->modelClassName);
        return str_replace(
            [
                '[id]',
                '[name]',
                '[label]',
                '[value]',
                '[required]',
                '[multiple]',
                '[model]',
                '[class]',
            ],
            [
                $formType->getId(),
                $formType->getName(),
                sprintf('trans("%s")', "{$this->langPath}.{$formType->getName()}"),
                "$$model->{$formType->getName()}",
                $formType->getRequired() ? 'true' : 'false',
                $formType->getMultiple() ? 'true' : 'false',
                '$modelOptions[\''.$formType->getId().'\']',
                $formType->getClasses()
            ],
            $this->fileManager->viewStub("forms/{$formType->getType()}")
        );
    }
}
