<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

use Foolskill\LaravelVCrudGenerator\Helper;

class ControllerGenerator extends BaseGenerator implements Generator
{
    public function generate()
    {
        try {
            $managerHelpers = Helper::normalizeManager($this->namespace, $this->modelClassName);
            $directory = Helper::makeDirectory(app_path("Http/Controllers/{$this->namespace}"));
            $baseModelClassName = Helper::baseClassname($this->modelClassName);

            foreach (['create', 'update', 'delete', 'list'] as $target) {
                $vName = ucfirst($target);
                // Récupère le code source du controller concerné
                // Remplace les placeholder dans le code source
                $template = str_replace(
                    [
                        '[modelClassNameWithNamespace]',
                        '[managerClassname]',
                        '[namespace]',
                        '[modelClassName]',
                        '[modelClassNameToLower]',
                        '[viewBaseName]',
                        '[trans]'
                    ],
                    [
                        $this->modelClassName,
                        'App\\Managers\\'.$managerHelpers['classname'],
                        $this->namespace,
                        $baseModelClassName,
                        lcfirst($baseModelClassName),
                        $this->viewBaseName,
                        $this->langPath,
                    ],

                    Helper::controllerStub($target)
                );

                // Copie le code dans le fichier correspondant
                Helper::putContent("{$directory}/{$vName}Controller.php", $template);
            }

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }
    }
}
