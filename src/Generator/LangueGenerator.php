<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

use Foolskill\LaravelVCrudGenerator\Helper;
use function Couchbase\defaultDecoder;

class LangueGenerator extends BaseGenerator implements Generator
{
    public function generate()
    {
        // TODO: Implement generate() method.
        try {

            $model = $this->getModel();
            $path = str_replace('.', '/', $this->viewBaseName);
            $file = Helper::baseDir($path);
            $path = str_replace('/'.$file, '', $path);
            $fileManager = new FileManager(
                resource_path("lang/fr/{$path}")
            );

            $rest = [];
            foreach ($model->formFields as $formField) {
                $rest[] = "'{$formField->name}' => '{$formField->label}',\r\n";
            }

            $template = str_replace(
                [
                    '[rest]'
                ],
                [
                    join("\t", $rest)
                ],
                Helper::getStubsContent('langue')
            );

            $fileManager->putOn("{$file}.php", $template);

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }
    }
}
