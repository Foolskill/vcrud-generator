<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

use Foolskill\LaravelVCrudGenerator\Helper;

class RouteGenerator extends BaseGenerator implements Generator
{
    public function generate()
    {
        // TODO: Implement generate() method.
        try {
            $this->oldGenerate();
        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }
    }

    private function newGenerator()
    {
        $path = app_path('routes/web/'.strtolower($this->namespace));
        $model = strtolower(Helper::baseClassname($this->modelClassName));
        $prefix = Helper::baseClassname($path);

        Helper::makeDirectory($p = trim(str_replace($prefix, '', $path), '\\'));

        $template = str_replace(
            [
                '[prefix]',
                '[route_name]',
                '[model]',
            ],
            [
                $prefix,
                $prefix,
                $model,
            ],
            Helper::routeStub()
        );

        dd("$p/$prefix.php");
        Helper::putContent("$p/$prefix.php", $template);
    }

    private function oldGenerate()
    {

        $path = app_path('routes/web/'.strtolower($this->namespace));
        $model = strtolower(Helper::baseClassname($this->modelClassName));
        $prefix = Helper::baseClassname($path);

        $modelClassToLower = lcfirst($this->modelClassName);
        $path = str_replace('.', '/', $this->viewBaseName);
        $file = Helper::baseDir($path);
        $path = str_replace('/'.$file, '', $path);
        $rootPath = dirname(app_path());
        //dd($path, $file);

        $this->fileManager = new FileManager("$rootPath/routes/web/$path/");

        $template = str_replace(
            [
                '[prefix]',
                '[route_name]',
                '[model]',
            ],
            [
                $prefix,
                $prefix,
                $model,
            ],
            Helper::routeStub()
        );

        $this->fileManager->putOn("{$file}.php", $template);
    }
}
