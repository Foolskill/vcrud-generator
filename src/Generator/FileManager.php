<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

class FileManager
{
    private $path;

    public function __construct($path)
    {
        $this->makeDir($path);
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    private function makeDir($path)
    {
        if (! is_dir($path)) {
            mkdir($path, 0777, true);
        }

        $this->setPath($path);
    }

    public function makeSimpleDir($dir)
    {
        if (! is_dir($this->path.'/'.$dir)) {
            mkdir($this->path.'/'.$dir, 0777, true);
        }
    }

    public function putOn($fileName, $content = null)
    {
        file_put_contents("{$this->path}/{$fileName}", $content);
    }

    public function appendOn($fileName, $content = null)
    {
        file_put_contents("{$this->path}/{$fileName}", $content, FILE_APPEND);
    }

    /**
     * @param $target
     * @return false|string|void
     */
    public function controllerStub($target)
    {
        if (! $target) {
            return ;
        }

        return file_get_contents(dirname(dirname(__DIR__)). "/resources/stubs/controllers/$target.stub");
    }

    /**
     * @param $target
     * @return false|string|void
     */
    public function formRequestStub($target)
    {
        if (! $target) {
            return ;
        }

        return file_get_contents(dirname(dirname(__DIR__)). "/resources/stubs/requests/$target.stub");
    }

    /**
     * @param $target
     * @return false|string|void
     */
    public function managerStub($target)
    {
        if (! $target) {
            return ;
        }

        return file_get_contents(dirname(dirname(__DIR__)). "/resources/stubs/$target.stub");
    }

    /**
     * @param $target
     * @return false|string|void
     */
    public function langueStub($target)
    {
        if (! $target) {
            return ;
        }

        return file_get_contents(dirname(dirname(__DIR__)). "/resources/stubs/$target.stub");
    }

    /**
     * @param $target
     * @return false|string|void
     */
    public function viewStub($target)
    {
        if (! $target) {
            return ;
        }

        return file_get_contents(dirname(dirname(__DIR__)). "/resources/stubs/views/$target.stub");
    }


    public function routeStub()
    {
        return file_get_contents(dirname(dirname(__DIR__)). "/resources/stubs/route.stub");
    }

}
