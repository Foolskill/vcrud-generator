<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

abstract class BaseGenerator
{
    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    protected $target;

    /**
     * @var
     */
    protected $namespace;

    /**
     * @var
     */
    protected $modelClassName;

    /**
     * @var
     */
    protected $viewBaseName;

    /**
     * @var
     */
    protected $langPath;

    /**
     * @var
     */
    protected $navLinkFile;

    /**
     * @var
     */
    protected $fileManager;

    /**
     * @param mixed $langPath
     */
    public function setLangPath($langPath)
    {
        $this->langPath = $langPath;
        return $this;
    }

    /**
     * @param $modelClassName
     * @return $this
     */
    public function setModelClassName($modelClassName)
    {
        $this->modelClassName = $modelClassName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModelClassName()
    {
        return $this->modelClassName;
    }

    /**
     * @param $navLinkFile
     * @return $this
     */
    public function setNavLinkFile($navLinkFile)
    {
        $this->navLinkFile = $navLinkFile;
        return $this;
    }

    /**
     * @param $namespace
     * @return $this
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * @param mixed $view
     */
    public function setViewBaseName($view)
    {
        $this->viewBaseName = $view;
        $this->setLangPath(str_replace('.', '/', $view));
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @param mixed $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $fileManager
     */
    public function setFileManager($fileManager)
    {
        $this->fileManager = $fileManager;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileManager()
    {
        return $this->fileManager;
    }
}
