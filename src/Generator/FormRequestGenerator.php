<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

use Foolskill\LaravelVCrudGenerator\Generator\Views\FormRequests\Attribute;
use Foolskill\LaravelVCrudGenerator\Helper;

class FormRequestGenerator extends BaseGenerator implements Generator
{
    protected $directory = 'App/Http/Requests';

    public function generate()
    {
        // TODO: Implement generate() method.

        try {

            $this->generateFormRequest();

            $this->generateCommonTrait();

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }
    }

    private function generateCommonTrait()
    {
        $path = "$this->directory/$this->namespace";
        // - Parcourir les champs de formulaires
        // - Récupérer la règle de validation de chaque champ de formulaire
        $rules = [];
        $attributes = [];

        foreach ($this->model->formFields as $formField) {

            $rules[] = sprintf(
                "'%s' => '%s', \n",
                $formField->name,
                $formField->validation_rules
            );

            $transLabel = Helper::viewBaseNameToTransLabel($this->viewBaseName);

            $attributes[] = sprintf(
                "'%s' => trans('%s'), \n",
                $formField->name,
                "{$transLabel}.{$formField->name}"
            );
        }

        $template = str_replace(
            [
                '[namespace]',
                '[modelClassName]',
                '##[attributes]',
                '##[rules]',
            ],
            [
                $this->namespace,
                $this->modelClassName,
                join("\t\t\t", $attributes),
                join("\t\t\t", $rules),
            ],

            Helper::formRequestStub('RequestCommonTrait')
        );

        Helper::putContent("$path/RequestCommonTrait.php", $template);

    }

    private function generateFormRequest()
    {
        $path = "$this->directory/$this->namespace";

        Helper::makeDirectory($path);
        $baseModelClassName = Helper::baseClassname($this->modelClassName);
        foreach (['Create', 'Update', 'Delete'] as $target) {
            $template = str_replace(
                [
                    '[namespace]',
                    '[modelClassNameWithNamespace]',
                    '[modelClassName]',
                    '[modelKey]',
                ],
                [
                    $this->namespace,
                    $this->modelClassName,
                    $baseModelClassName,
                    lcfirst($baseModelClassName),
                ],
                Helper::formRequestStub($target, 'Request')
            );

            Helper::putContent("$path/{$target}Request.php", $template);
        }
    }

    private function create()
    {
        try {

            $fileManager = new FileManager(
                app_path("Http/Requests/{$this->namespace}")
            );

            $template = str_replace(
                [
                    '[namespace]',
                ],
                [
                    $this->namespace,
                ],
                $fileManager->formRequestStub('CreateRequest')
            );

            $fileManager->putOn("CreateRequest.php", $template);

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }

    }

    private function update()
    {
        try {

            $fileManager = new FileManager(
                app_path("Http/Requests/{$this->namespace}")
            );

            $template = str_replace(
                [
                    '[namespace]',
                ],
                [
                    $this->namespace,
                ],
                $fileManager->formRequestStub('UpdateRequest')
            );

            $fileManager->putOn("UpdateRequest.php", $template);

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }

    }

    private function delete()
    {
        try {

            $fileManager = new FileManager(
                app_path("Http/Requests/{$this->namespace}")
            );

            $template = str_replace(
                [
                    '[namespace]',
                ],
                [
                    $this->namespace,
                ],
                $fileManager->formRequestStub('DeleteRequest')
            );

            $fileManager->putOn("DeleteRequest.php", $template);

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }

    }

    private function oldGenerateCommonRequest()
    {
        $model = $this->getModel();
        $attribute = [];
        $rules = [];
        $trans = str_replace('.', '/', $this->viewBaseName);

        $formRequestAttribute = new Attribute();
        foreach ($model->formFields as $formField) {

            $attribute[] = $formRequestAttribute->setKey($formField->name)->setLangPath($trans)->generate();

            $requiredRule = $formField->required ? 'required' : 'nullable';

            $rules[] = sprintf("'%s' => '%s', \n", $formField->name, $requiredRule);
        }

        foreach (['CommonTrait'] as $item) {

            $template = str_replace(
                [
                    '[namespace]',
                    '[modelClassName]',
                    '##[attributes]',
                    '##[rules]',
                ],
                [
                    $this->namespace,
                    $this->modelClassName,
                    join("\t\t\t", $attribute),
                    join("\t\t\t", $rules),
                ],
                Helper::getStubsContent("requests/{$item}Request")
            );

            $this->fileManager->putOn("{$item}Request.php", $template);
        }
    }
}
