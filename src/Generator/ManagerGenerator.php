<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

use Foolskill\LaravelVCrudGenerator\Helper;

class ManagerGenerator extends BaseGenerator implements Generator
{
    public function generate()
    {
        try {
            $managerHelpers = Helper::normalizeManager($this->namespace, $this->modelClassName);
            $directory = Helper::makeDirectory(app_path('Managers'));
            $relationsMethodLoader = $this->relationsMethodLoader();
            $baseModelClassName = Helper::baseClassname($this->modelClassName);
            $template = str_replace(
                [
                    '[managerNamespace]',
                    '[managerClassname]',
                    '[modelClassNameWithNamespace]',
                    '[namespace]',
                    '[modelClassesNamespace]',
                    '[modelClassName]',
                    '[modelClassNameToLower]',
                    '[relations]',
                    '[formFields]',
                ],
                [
                    $managerHelpers['namespace'],
                    $managerHelpers['classname'],
                    $this->modelClassName,
                    $this->namespace,
                    join("\n", $relationsMethodLoader['namespaces']),
                    $baseModelClassName,
                    lcfirst($baseModelClassName),
                    join("\n\t\t\t", $relationsMethodLoader['returnedArray']),
                    join("\n\t\t\t", $this->formFieldsCode()),
                ],
                Helper::managerStub()
            );

            Helper::putContent("{$directory}/{$managerHelpers['classname']}.php", $template);

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }
    }

    private function relationsMethodLoader(): array
    {
        $namespaces = [];
        $returnedArray = [];
        foreach ($this->model->formFields as $formField) {
            if ($formField->model) {
                $namespaces[] = "use {$formField->model};";
                $returnedArray[] = $this->relationCode($formField);
            }
        }

        return compact('namespaces', 'returnedArray');
    }

    private function relationCode($formField): string
    {
        return sprintf(
            "'%s' => %s::query()->orderBy('%s')->pluck('%s', '%s')->prepend(null, ''),"
            ,
            $formField->id,
            Helper::modelClassWithoutNamespace($formField->model),
            $formField->model_via,
            $formField->model_via,
            $formField->model_key
        );
    }

    private function formFieldsCode($groupName = 'target'): array
    {
        $formFields = [];
        $groupList = collect($this->model->formFields)
            ->filter(function ($item) use ($groupName) {
                return $item->{$groupName};
            })
        ;

        foreach ($groupList->toArray() as $index => $formField) {

            $formFields[] = sprintf(
                "'%s' => %s('%s'),",
                $formField->name,
                $formField->type == 'checkbox' ? '$request->filled' : '$request->post',
                $formField->name
            );
        }
        return $formFields;
    }

}
