<?php

namespace Foolskill\LaravelVCrudGenerator\Generator;

use Foolskill\LaravelVCrudGenerator\Helper;

class ViewGenerator extends BaseGenerator implements Generator
{
    /**
     * @var FormsGenerator
     */
    private $formsGenerator;

    /**
     * @var TableGenerator
     */
    private $tableGenerator;

    private $directories = [
        'partials' => 'partials',
        'forms' => 'partials/forms',
        'cards' => 'partials/forms/cards',
    ];

    private $modelRoot;

    public function __construct()
    {
        $this->formsGenerator = new FormsGenerator();
        $this->tableGenerator = new TableGenerator();
    }

    public function generate()
    {
        // TODO: Implement generate() method.
        try {

            $this->modelRoot = resource_path(
                'views'.DIRECTORY_SEPARATOR.str_replace('.', DIRECTORY_SEPARATOR, $this->viewBaseName)
            );

            $this->makeAllViewsDirectories();
            $this->preparePartialsFiles();
            $this->generatePartialsCardsView();
            $this->hydrateModalDeleteForm();

            $this->generateMainView();
            $this->generateNavLink();

            //$this->makeSubmitButtonForm();

            // ici deployer les partials.
            $this
                ->formsGenerator
                ->setFileManager($this->fileManager)
                ->setModel($this->getModel())
                ->setNamespace($this->namespace)
                ->setViewBaseName($this->viewBaseName)
                ->setModelClassName($this->modelClassName)
                //->generate()
            ;

            $this
                ->tableGenerator
                ->setFileManager($this->fileManager)
                ->setModel($this->getModel())
                ->setNamespace($this->namespace)
                ->setViewBaseName($this->viewBaseName)
                ->setModelClassName($this->modelClassName)
                //->generate()
            ;

            //$this->details();

        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
        }
    }

    private function makeAllViewsDirectories()
    {
        foreach ($this->directories as $directory) {
            Helper::makeDirectory($this->modelRoot.DIRECTORY_SEPARATOR.$directory);
        }
    }

    private function preparePartialsFiles()
    {
        Helper::putContent("{$this->modelRoot}/{$this->directories['partials']}/list-table.blade.php", '');
        Helper::putContent("{$this->modelRoot}/{$this->directories['forms']}/simple-filter.blade.php", '');
    }

    private function generatePartialsCardsView()
    {
        $includedFiles = [];
        foreach ($this->model->cards as $card) {
            // Génération des champs de formulaires
            Helper::putContent(
                "{$this->modelRoot}/{$this->directories['forms']}/{$card['file-name']}.blade.php",
                str_replace(
                    [
                        '[code]',
                    ],
                    [
                        $this->formFields($card),
                    ],
                    Helper::viewStubDirectory('partials/card')
                )
            );

            $includedFiles[] = str_replace(
                [
                    '[panel_css_classes]',
                    '[css_classes]',
                    '[namespace]',
                    '[card_trans_title]',
                    '[code]',
                ],
                [
                    $card['panel_css_classes'] ?? '',
                    $card['file-name'] ?? '',
                    $this->langPath,
                    $card['card_trans_title'],
                    "@include('{$this->viewBaseName}.partials.forms.{$card['file-name']}')",
                ],

                $this->getFormCardTemplate()
            );

        }

        // Génération du formulaire principale
        $formTemplate = str_replace(
            [
                '[include_files]'
            ],
            [
                implode("\n\n", $includedFiles),
            ],
            Helper::viewStubDirectory('partials/form')
        );

        Helper::putContent(
            "{$this->modelRoot}/{$this->directories['partials']}/form.blade.php",
            $formTemplate
        );

    }

    private function getFormCardTemplate(): string
    {
        $html = [
            "\t<div class='[panel_css_classes]'>",
                "\t\t<div class='card shadow [css_classes]'>",
                        "\t\t\t<div class='card-body'>",
                            "\t\t\t\t[code]",
                        "\t\t\t</div>",
                "\t\t</div>",
            "\t</div>",
        ];

        return implode("\n", $html);
    }

    private function formFields($card): string
    {
        $formFields = collect($this->model->formFields)->filter(function ($item) use ($card) {
            return $item->card == $card['id'] ;
        });

        $modelVariable = lcfirst(Helper::baseClassname($this->modelClassName));
        $codes = [];
        foreach ($formFields as  $formField) {
            $codes[] = $this->getInputFormTypeCodeSource($formField, $modelVariable);
        }

        return implode("", $codes);
    }

    private function getInputFormTypeCodeSource($formField, $modelVariable)
    {
        return str_replace(
            [
                '[name]',
                '[label]',
                '[model]',
                '[value]',
                '[class]',
                '[id]',
                '[multiple]',
                '[required]',
            ],
            [
                $formField->name,
                $this->langPath.'.'.$formField->name,
                $formField->id,
                "$".$modelVariable.'->'.$formField->name,
                $formField->classes,
                $formField->id,
                ! empty($formField->multiple) ? 'true' : 'false',
                ! empty($formField->required) ? 'true' : 'false',
            ],
            Helper::viewStubDirectory("forms/{$formField->type}")
        );
    }

    private function populateFiles(): bool
    {
        // Form et card (stub partials/forms.stub)
        return true;
    }

    /**
     * Make de mains views (create, edit, list)
     */
    private function generateMainView()
    {
        foreach (['create', 'edit', 'list'] as $fileName) {
            $template = str_replace(
                [
                    '[layout]',
                    '[namespace]',
                    '[viewBaseName]',
                    '[modelClassName]',
                    '[modelVariable]',
                ],
                [
                    config('vcrud-generator.layout'),
                    $this->langPath,
                    $this->viewBaseName,
                    lcfirst($this->modelClassName),
                    strtolower(basename($this->model->modelClassName))
                ],
                Helper::viewStubDirectory($fileName)
            );

            $path = "{$this->modelRoot}/{$fileName}.blade.php";
            Helper::putContent($path, $template);
        }
    }

    private function hydrateModalDeleteForm()
    {
        // Modal delete
        $template = str_replace(
            [
                '[namespace]',
                '[viewBaseName]',
                '[modelClassName]'
            ],
            [
                $this->langPath,
                $this->viewBaseName,
                lcfirst(Helper::baseClassname($this->modelClassName)),
            ],
            Helper::viewStubDirectory('forms/delete')
        );

        Helper::putContent("{$this->modelRoot}/{$this->directories['forms']}/modal-delete.blade.php", $template);
    }

    private function generateNavLink()
    {
        if (empty($this->model->navLinkFile)) {
            return false;
        }

        $file = resource_path("views/{$this->model->navLinkFile}");
        if (file_exists($file)) {
            Helper::putContent($file.'.backup', file_get_contents($file));
            $template = str_replace(
                [
                   "<!--code-->"
                ],
                [
                    $this->navLink()
                ],
                file_get_contents($file)
            );

            Helper::putContent($file, $template);
        }
    }

    private function navLink()
    {
        $trans = Helper::viewBaseNameToTransLabel($this->viewBaseName);
        $fa = !empty($this->model->navLinkIconClass) ? $this->model->navLinkIconClass : null;
        $html = [
            "<a class='nav-link {{ \Route::is('$this->viewBaseName.*') ? 'active' : null }}' href='{{ route('$this->viewBaseName.list') }}'>",
            "\t\t\t<i class='$fa'></i>{!! trans('$trans.title_link') !!}",
            "\t\t</a>",
            "\t\t<!--code-->",
        ];


        return implode("\n", $html);
    }

    private function makeSubmitButtonForm()
    {
        // Modal delete
        $template = str_replace(
            [
                '[viewBaseName]',
            ],
            [
                $this->viewBaseName,
            ],
            Helper::getStubsContent('views/forms/submit')
        );

        $this->fileManager->putOn("partials/forms/submit.blade.php", $template);
    }
}
