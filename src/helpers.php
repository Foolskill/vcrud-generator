<?php

if (! function_exists('get_stubs_content')) {
    function stub_file($target) {
        return dirname(__DIR__). "/resources/stubs/{$target}.stub";
    }


}

function get_stubs_content($target) {
    return file_get_contents(dirname(__DIR__). "/resources/stubs/{$target}.stub");
}
