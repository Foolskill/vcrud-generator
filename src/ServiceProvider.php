<?php

namespace FoolSkill\LaravelVCrudGenerator;

use Foolskill\LaravelVCrudGenerator\Console\Commands\VCrud;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Support\Facades\Artisan;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * @var string
     */
    protected $configFileName = 'vcrud-generator';

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__."/../config/{$this->configFileName}.php", $this->configFileName);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootConfig();
        $this->bootMigrations();
        $this->bootRoutes();
        $this->bootTranslations();
        $this->bootViews();
        $this->bootAssets();

        if ($this->app->runningInConsole()) {
            // command here
            $this->bootCommands();
        }
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function bootConfig()
    {
        $this->publishes([
            __DIR__."/../config/{$this->configFileName}.php" => config_path("{$this->configFileName}.php"),
        ], 'config');

    }


    /**
     * Register console commands.
     *
     * @return void
     */
    protected function bootCommands()
    {
        $this->commands([
            VCrud::class
        ]);
    }

    /**
     * Boot database migrations.
     *
     * @return void
     */
    protected function bootMigrations()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations/');
    }


    protected function bootRoutes()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        $this->publishes([
            __DIR__.'/../routes' => base_path("routes/vendor/{$this->configFileName}"),
        ], 'users-access-routes');
    }

    protected function bootTranslations()
    {
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', $this->configFileName);

        $this->publishes([
            __DIR__.'/../resources/lang' => $this->app->langPath("vendor/{$this->configFileName}"),
        ], 'lang');
    }

    protected function bootViews()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', $this->configFileName);

        $this->publishes([
            __DIR__.'/../resources/views' => resource_path("views/vendor/$this->configFileName"),
        ], 'views');
    }

    protected function bootAssets()
    {
        $this->publishes([
            __DIR__.'/../public' => public_path("vendor/$this->configFileName"),
        ], 'public');
    }

}
