<?php

namespace Foolskill\LaravelVCrudGenerator\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormField extends Model
{
    use SoftDeletes;

    protected $table = 'vcrud_form_group_fields';
    protected $guarded = ['id'];
}
