<?php

namespace Foolskill\LaravelVCrudGenerator\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VCrud extends Model
{
    use SoftDeletes;

    protected $table = 'vcrud';
    protected $guarded = ['id'];

    public function formsGroups()
    {
        return $this->hasMany(FormGroup::class, 'vcrud_id');
    }
}
