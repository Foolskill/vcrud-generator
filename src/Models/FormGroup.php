<?php

namespace Foolskill\LaravelVCrudGenerator\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormGroup extends Model
{
    use SoftDeletes;

    protected $table = 'vcrud_forms_groups';
    protected $guarded = ['id'];

    public function formsFiels()
    {
        return $this->hasMany(FormField::class);
    }
}
