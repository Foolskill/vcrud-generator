<?php

namespace Foolskill\LaravelVCrudGenerator\Http\Controllers;

class ListController extends Controller
{
    public function index()
    {
        return view('vcrud-generator::list');
    }
}
