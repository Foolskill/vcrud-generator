<?php

namespace Foolskill\LaravelVCrudGenerator\Console\Commands;

use Foolskill\LaravelVCrudGenerator\Generator\CodeGenerator;
use Foolskill\LaravelVCrudGenerator\Generator\ControllerGenerator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class VCrud extends Command
{
    protected $signature = 'vcrud:g';

    protected $description = 'Create CRUD operations';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(CodeGenerator $codeManager)
    {
        //$model = \Foolskill\LaravelVCrudGenerator\Models\VCrud::first();
        //dd(config('vcrud-generator.data'));

        foreach (config('vcrud-generator.data') as $data) {

            if ($data['locked']) {
                //$this->error("{$data['namespace']} a été verrouillé");
                continue;
            }

            $this->line("Génération de {$data['namespace']}");

            $model = new \stdClass;
            $model->namespace = $data['namespace'];
            $model->modelClassName = $data['modelClassName'];
            $model->viewBaseName = $data['viewBaseName'];
            $model->navLinkFile = $data['navLinkFile'] ?? null;
            $model->navLinkIconClass = $data['navLinkIconClass'] ?? null;

            $card = new \stdClass;
            $card->name = "Main";
            $model->cards = $data['cards'] ?? [];

            $formsFields = [];
            foreach ($data['formsFields'] as $fields) {
                $formField = new \stdClass;
                foreach ($fields as $key => $value) {
                    $formField->{$key} = $value;
                }
                $formsFields[] = $formField;
            }

            $model->formFields = $formsFields;

            $codeManager
                ->setModel($model)
                ->generate();

            $this->info("{$data['namespace']} généré");
        }

    }
}
