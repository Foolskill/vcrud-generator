<?php

namespace Foolskill\LaravelVCrudGenerator;

class Helper
{
    public static function getStubsContent($target)
    {
        return file_get_contents(dirname(__DIR__). "/resources/stubs/{$target}.stub");
    }

    public static function transPathFromNamespace($namespace)
    {
        return strtolower(str_replace('\\', '/', $namespace));
    }

    public static function viewPathFromNamespace($baseName)
    {
        return strtolower(str_replace('\\', '/', $baseName));
    }

    /**
     * @param $dir
     * @return mixed|string
     */
    public static function baseDir($dir)
    {
        $split = explode('/', $dir);

        return $split[count($split) - 1];
    }

    public static function modelClassWithoutNamespace($namespace)
    {
        $split = explode('\\', $namespace);

        return $split[count($split) - 1];
    }

    public static function makeDirectory($path)
    {
        if (! is_dir($path)) {
            mkdir($path, 0777, true);
            return $path;
        }

        return $path;
    }

    /**
     * @param $target
     * @return false|string
     */
    public static function controllerStub($target)
    {
        $className = ucfirst($target).'Controller';
        $path = config('vcrud-generator.stub-directory')."/controllers/$className.stub";
        if (! file_exists($path)) {
            return false;
        }

        return file_get_contents($path);
    }

    /**
     * @param $target
     * @return false|string
     */
    public static function routeStub()
    {
        $path = config('vcrud-generator.stub-directory')."/route.stub";
        if (! file_exists($path)) {
            return false;
        }

        return file_get_contents($path);
    }

    /**
     * @param $target
     * @return false|string
     */
    public static function formRequestStub($target, $suffix = '')
    {
        $className = ucfirst($target).$suffix;
        $path = config('vcrud-generator.stub-directory')."/requests/$className.stub";
        if (! file_exists($path)) {
            return false;
        }

        return file_get_contents($path);
    }

    /**
     * @param $target
     * @return false|string
     */
    public static function managerStub()
    {
        $path = config('vcrud-generator.stub-directory')."/ModelManager.stub";
        if (! file_exists($path)) {
            return false;
        }

        return file_get_contents($path);
    }

    /**
     * @param $target
     * @return false|string
     */
    public static function viewStubDirectory($fileName)
    {
        $path = config('vcrud-generator.stub-directory')."/views/$fileName.stub";
        if (! file_exists($path)) {
            return false;
        }

        return file_get_contents($path);
    }

    /**
     * @return false|string
     */
    public static function repositoryStub()
    {
        $path = config('vcrud-generator.stub-directory')."/ModelRepository.stub";
        if (! file_exists($path)) {
            return false;
        }

        return file_get_contents($path);
    }

    /**
     * @return false|string
     */
    public static function navLinkStub()
    {
        $path = config('vcrud-generator.stub-directory')."/nav-link.stub";
        if (! file_exists($path)) {
            return false;
        }

        return file_get_contents($path);
    }

    /**
     * @param $namespace
     * @param $modelClassname
     * @return array
     */
    public static function normalizeManager($namespace, $modelClassname): array
    {
        $split = explode('\\', $namespace);
        $baseClassname = self::baseClassname($modelClassname);
        $baseNamespace = 'Managers\\'.str_replace('\\'.$split[count($split) - 1], '', $namespace);
        return [
            'namespace' => 'App\\'.$baseNamespace,
            'classname' => $baseClassname.'Manager',
            'directory' => app_path($baseNamespace),
            'original_namespace' => $split,
        ];
    }

    /**
     * @param $modelClassname
     * @return string
     */
    public static function baseClassname($modelClassname)
    {
        return basename($modelClassname);
    }

    /**
     * @param $path
     * @param null $content
     */
    public static function putContent($path, $content = null)
    {
        file_put_contents($path, $content);
    }


    /**
     * @param $viewBaseName
     * @return array|string|string[]
     */
    public static function viewBaseNameToTransLabel($viewBaseName)
    {
        return str_replace('.', '/', $viewBaseName);
    }
}
